# Read.me
### _written by Alexey Tarasik for personal use_

### Task

> Change ntp to ntp.se with Ansible in an RHEL 8 virtual server named ntp.edu.tentixo.com via VagrantBox on VirtualBox - connection should be done with computer name, not IP.


This file describes a set of sequential steps to change the ntp server to ntp.se using Vagrant and VirtualBox for RHEL8. Server configuration will be done using Ansible.

+ [More about VirtualBox]
+ [More about VirtualBox by Wiki]
+ [More about Vagrant]
+ [More about Vagrant by Wiki]
+ [More about RHEL8]
+ [More about RHEL8 by Wiki]
+ [More about Ansible]
+ [More about Ansible by Wiki]

#### Objective of the project

> The purpose of the work is to get practical skills in working with Ansible, Vagrant, VirtualBox, RHEL8, using the example of automating the process of configuring an ntp server.

If you are using a Linux operating system, then first you need to install Vagrant and VirtualBox. 

### Installation
For install Vagrant and Virtual box you can use next lines in your terminal window (using ctrl+alt+T):
```sh
    $ sudo apt update
    $ sudo apt install virtualbox  
    $ curl -O https://releases.hashicorp.com/vagrant/2.2.19/vagrant_2.2.19_x86_64.deb   
    $ sudo apt install ./vagrant_2.2.9_x86_64.deb
```
I installed Vagrant version 2.2.9. VirtualBox version is 6.1.30 r148432 (Qt5.12.8).
After installing Vagrant and VirtualBox, you need to create a separate folder that will contain the necessary documents. 
```ch
    $ mkdir VagrantVM
    $ cd VagrantVM
```
In my case: "/home/aleksey/VagrantVM". After that, you need to create virtualenv:
```ch
    $ cd VagrantVM
    $ python3 -m venv VagrantVM
    $ source VagrantVM/bin/activate
    $ pip install -r requirements.txt
```
If you need more information about using virtualenv, you can look for there _[click]_.
After that, you need to run the following lines in your created folder (_[additional information]_):
```ch
    $ vagrant init generic/rhel8
    $ vagrant up --provider=virtualbox
```
For _connection_ to virtual machine you need to use next line:
```ch
    $ vagrant ssh ntp.edu.tentixo.com
```
After you need to configure VagrantFile.
##### About VagrantFile
The primary function of the Vagrantfile is to describe the type of machine required for a project, and how to configure and provision these machines. Vagrant is meant to run with one Vagrantfile per project, and the Vagrantfile is supposed to be committed to version control. 
To configure this file, you can use the following sites:
- [Vagrantfile]
- [Vagrantfile Examples]

My VagrantFile looks like this:
```ch
#Vagrantfile
Vagrant.configure("2") do |config|                      
  config.vm.define "ntp.edu.tentixo.com"            #Description of server name and config name
  config.vm.hostname = "ntp.edu.tentixo.com"        #Hostname definition
  config.vm.box = "generic/rhel8"                   #Specifying the image that the vagrant will use and deploy
  config.vm.box_check_update = false                #Prevent checking for updates for an image
  config.vm.network "private_network", ip: "192.168.56.5"   #Description of the network type, specifying the ip of the machine
  
  config.vm.provider "virtualbox" do |vb|       #Provider selection and its config
    vb.gui = false                              #Boot without a GUI
    vb.name = "RHEL8_vm"                        #Virtual Machine Name
    vb.memory = "1024"                          #Allocating memory for VM
    vb.cpus = "2"                               #Allocating cpu for VM
  end
  
  config.vm.provision "ansible" do |ansible|
    ansible.playbook = "playbook.yml"           # Path to playbook.yml file
    ansible.inventory_path = "inventory"        # Path to the inventory file
  end
end
```
##### Ansible Installation Instructions
After configuration Vagrantfile, you need to install Ansible and create Playbook.yml in the same directory where VagrantFile was initialized.
The following sites will help you in this task:
- [Install and Setup Ansible]
- [Python 3 Support]
- [Installing Ansible]  

Or you can using this lines:
```ch
    $ sudo apt update
    $ sudo apt install software-properties-common
    $ sudo add-apt-repository --yes --update ppa:ansible/ansible
    $ sudo apt install ansible
```
###### About Playbook.yaml
Playbooks are one of the core features of Ansible and tell Ansible what to execute. They are like a to-do list for Ansible that contains a list of tasks.
Playbooks contain the steps which the user wants to execute on a particular machine. Playbooks are run sequentially. Playbooks are run sequentially.
The following sites can help you configure your playbook:
- [NTP configuration using Ansible]
- [Ansible Documentation]
- [Ansible lineinfile regexp to manage /etc/exports]
- [Examples of working with Ansible]

Or you can using this lines:
```ch
#playbook.yml
---
- hosts: ntp                #The name of the group of machines on which this playbook will be executed
  become: true              #All tasks will be executed as root
  tasks:
    - name: Update OS       #Update all packages to their latest version
      package:
        name: '*'
        state: latest
    - name: Сhecking the work Chrony
      service: name=chronyd state=started enabled=yes
      tags: chrony
    - name: Replace line in config file     #Replacing the standard server pool with ntp.se servers
      lineinfile:
        path: /etc/chrony.conf
        regexp: '{{ item.regexp }}'         #Search for a string with a given name
        line: '{{ item.line }}'             #Replacing the found string with the string specified here
        backrefs: yes                       #If the line is not found, then the file will not be changed
      with_items:
        - { regexp: '^pool(.*)', line: 'server lul1.ntp.se \n server lul2.ntp.se \n server svl2.ntp.se \n server svl1.ntp.se' }
    - name: Restart                         #Rebooting the service after changing the configuration file
      shell: systemctl restart chronyd.service
```

###### About inventory
Ansible uses an inventory file to keep track of which hosts are part of your infrastructure, and how to reach them for running commands and playbooks. For Ansible to automate a Linux Server, Network device or Cloud server it has to exist within the inventory (also known as the Ansible hosts file) and saved in either YAML or INI format.
```ch
#inventory
[ntp]
ntp.edu.tentixo.com 
ansible_ssh_host=127.0.0.1    #The name of the host to connect to, if different from the alias you wish to give to it
ansible_ssh_port=2222         #The connection port number, if not the default (22 for ssh)
ansible_ssh_user=vagrant      #The user name to use when connecting to the host
ansible_ssh_private_key_file==/home/aleksey/VagrantVM/.vagrant/machines/ntp.edu.tentixo.com/virtualbox/private_key  #Private key file used by ssh.
```
You can read more information about this on the following pages:
- [How To Set Up Ansible Inventories]
- [What is the Ansible Inventory File?]

##### Cheking the work of ntp servers:
```ch
  $ chronyc sources
  $ chronyc tracking
```

###### About requirements.txt
In the file requirements.txt, there is a list of all installed packages. This file is required to transfer the installed packages to another Linux environment.
- [Website about requirements.txt]

**Thank you for watching**

   [More about VirtualBox]: https://www.virtualbox.org/
   [More about VirtualBox by Wiki]: https://en.wikipedia.org/wiki/VirtualBox
   [More about Vagrant]: https://www.vagrantup.com/intro
   [More about Vagrant by Wiki]: https://en.wikipedia.org/wiki/Vagrant_(software)
   [More about RHEL8 ]: https://www.redhat.com/en/enterprise-linux-8
   [More about RHEL8 by Wiki]: https://en.wikipedia.org/wiki/Red_Hat_Enterprise_Linux
   [More about Ansible]: https://docs.ansible.com/ansible/latest/index.html
   [More about Ansible by Wiki]: https://en.wikipedia.org/wiki/Ansible_(software)
   [How to Install Vagrant on CentOS 8]: https://linuxize.com/post/how-to-install-vagrant-on-centos-8/
   [How to install Vagrant on CentOS 8 / RHEL 8]: https://jasa-pengukuran.com/how-to-install-vagrant-on-centos-8-rhel-8/
   [click]: https://blog.sedicomm.com/2021/06/29/chto-takoe-virtualenv-v-python-i-kak-ego-ispolzovat/
   [additional information]: https://el8-lab.all-open.com/setup/vagrant/
   [Vagrantfile]: https://www.vagrantup.com/docs/vagrantfile
   [Vagrantfile Examples]: https://cmdref.net/software/vargant/vagrantfile
   [Install and Setup Ansible]: https://kifarunix.com/install-and-setup-ansible-on-ubuntu-20-04/
   [Python 3 Support]: https://docs.ansible.com/ansible/latest/reference_appendices/python_3_support.html
   [Installing Ansible]: https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-ansible-on-rhel-centos-or-fedora
   [NTP configuration using Ansible]: https://unixcop.com/install-configure-ntp-on-multiple-centos-7-8-servers-using-ansible/
   [Ansible Documentation]: https://docs.ansible.com/ansible/latest/index.html
   [Ansible lineinfile regexp to manage /etc/exports]: https://stackoverflow.com/questions/68522061/ansible-lineinfile-regexp-to-manage-etc-exports
   [Examples of working with Ansible]: https://www.dmosk.ru/miniinstruktions.php?mini=ansible-examples
   [How To Set Up Ansible Inventories]: https://www.digitalocean.com/community/tutorials/how-to-set-up-ansible-inventories
   [What is the Ansible Inventory File?]: https://www.rogerperkin.co.uk/network-automation/ansible/inventory-file/
   [Website about requirements.txt]: https://blog.sedicomm.com/2021/06/29/chto-takoe-virtualenv-v-python-i-kak-ego-ispolzovat/
